#! /usr/local/bin/bash
SERVER_FILE=proxmox_servers.yaml
mapfile -t SERVER < <(yq 'keys' ${SERVER_FILE} -o p | cut -f 3 -d ' ')

for SERVER_DATA in "${SERVER[@]}"
do
    eval API_TOKEN='$'$(pathEnv=".${SERVER_DATA}.api_token" yq 'eval(strenv(pathEnv))' ${SERVER_FILE})
    rm -f "inventory/${SERVER_DATA}.yaml"
    API_AUTH="PVEAPIToken=${TF_VAR_api_user}=${API_TOKEN}"
    SERVER_IP_ADDRESS=$(pathEnv=".${SERVER_DATA}.ip_address" yq 'eval(strenv(pathEnv))' ${SERVER_FILE})
    SERVER_NAME=$(pathEnv=".${SERVER_DATA}.name" yq 'eval(strenv(pathEnv))' ${SERVER_FILE})
    API_URL="https://${SERVER_IP_ADDRESS}:8006/api2/json"

    if [[ $(curl -skf "${API_URL}" -H "Authorization: ${API_AUTH}") ]]; then
        echo "Server ${SERVER_DATA} alive and want to share info"
    else
        echo "*** Server ${SERVER_DATA} burn in hell!!! ***"
        continue
    fi

    if [[ ! -f "inventory/${SERVER_DATA}.yaml" ]]; then
        echo "Making inventory file for ${SERVER_DATA}"
        touch "inventory/${SERVER_DATA}".yaml
    fi


    echo "Let's check $SERVER_NAME node on $SERVER_IP_ADDRESS"
    
    mapfile -t VM_ID < <(curl -ks -H "Authorization: ${API_AUTH}" -s \
                        "${API_URL}"/nodes/"${SERVER_NAME}"/qemu  | \
                        jq .data[].vmid | \
                        grep -v "9000|9001")

    echo "Found ${#VM_ID[@]} vms on ${SERVER_DATA}"

    for ID in "${VM_ID[@]}" 
    do

    VM_NAME=$(curl -ks -H "Authorization: ${API_AUTH}" \
        "${API_URL}"/nodes/"${SERVER_NAME}"/qemu/"${ID}"/agent/get-host-name | \
        yq '.data.result.host-name')

    VM_IP=$(curl -ks -H "Authorization: ${API_AUTH}" \
        "${API_URL}"/nodes/"${SERVER_NAME}"/qemu/"${ID}"/agent/network-get-interfaces | \
        yq  '.data.result[] | select(.name == "eth0") | .ip-addresses[] | select(.ip-address-type == "ipv4") | .ip-address')

    echo "$VM_NAME $VM_IP"
    pathEnv=".${SERVER_DATA}.children.$(echo "${VM_NAME}" | cut -d "-" -f 2).hosts.${VM_NAME//-/_}.ansible_host" valueEnv="$VM_IP" \
        yq 'eval(strenv(pathEnv)) = env(valueEnv)' -P -i "inventory/${SERVER_DATA}".yaml

    if [[ ! -d "host_vars/${VM_NAME//-/_}" ]]; then
        mkdir "host_vars/${VM_NAME//-/_}"
        touch "host_vars/${VM_NAME//-/_}/general.yaml"
    fi
    done

done